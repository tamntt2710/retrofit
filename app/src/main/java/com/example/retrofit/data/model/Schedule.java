package com.example.retrofit.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Schedule {
    @SerializedName("date")
    @Expose
    private Long date;
    @SerializedName("lessons")
    @Expose
    private List<Lesson> lessons = null;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "date=" + date +
                ", lessons=" + lessons +
                '}';
    }
}
