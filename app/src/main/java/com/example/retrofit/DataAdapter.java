package com.example.retrofit;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofit.data.model.Lesson;
import com.example.retrofit.data.model.SOAnswersResponse;
import com.example.retrofit.data.model.Schedule;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder>{
   public ArrayList<Lesson> arrayLisLesson ;

    public DataAdapter(ArrayList<Lesson> arrayLisLesson) {
        this.arrayLisLesson = arrayLisLesson;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view,parent,false);
        return new ViewHolder(view);
    }
//    public static String getDate(long milliSeconds, String dateFormat)
//    {
//        // Create a DateFormatter object for displaying date in specified format.
//        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
//
//        // Create a calendar object that will convert the date and time value in milliseconds to date.
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(milliSeconds);
//        return formatter.format(calendar.getTime());
//    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtmonhoc.setText(arrayLisLesson.get(position).getLesson());
        holder.txttiethoc.setText(arrayLisLesson.get(position).getSubjectName());
        holder.txtdiadiem.setText(arrayLisLesson.get(position).getAddress());
    }
    @Override
    public int getItemCount() {
        return arrayLisLesson.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

       private TextView txttiethoc;
       private TextView txtmonhoc;
       private TextView txtdiadiem;
       public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txttiethoc = (TextView)itemView.findViewById(R.id.tiethoc);
            txtmonhoc = (TextView)itemView.findViewById(R.id.monhoc);
            txtdiadiem =(TextView) itemView.findViewById(R.id.diadiem);

        }
    }
}
