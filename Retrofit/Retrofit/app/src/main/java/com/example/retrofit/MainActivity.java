package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retrofit.data.model.Lesson;
import com.example.retrofit.data.model.SOAnswersResponse;
import com.example.retrofit.data.model.Schedule;
import com.example.retrofit.data.remote.RetrofitClient;
import com.example.retrofit.data.remote.SOService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private SOService mSOService;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.textviewResult);


        mSOService = ApiUtils.getSOService();

        mSOService.getAnswers().enqueue(new Callback<SOAnswersResponse>() {
            @Override
            public void onResponse(Call<SOAnswersResponse> call, Response<SOAnswersResponse> response) {
                if (!response.isSuccessful()) {
                    textViewResult.setText("code" + response.code());
                    return;
                }

                SOAnswersResponse soAnswersResponse = response.body();
                List<Schedule> schedules = new ArrayList<Schedule>();
                final String[] content = {""};
                content[0] += "Ho va ten :  " + soAnswersResponse.getName() + "\n";
                content[0] += "Lop hoc : " + soAnswersResponse.getClassName() + "\n";
                content[0] += "Chuyen nganh :  " + soAnswersResponse.getMajor()+"\n";

                textViewResult.append(content[0]);
            }

            @Override
            public void onFailure(Call<SOAnswersResponse> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }


        });




    }
}
