package com.example.retrofit.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lesson {
    @SerializedName("lesson")
    @Expose
    private String lesson;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("address")
    @Expose
    private String address;

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "lesson='" + lesson + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
