package com.example.retrofit;

import com.example.retrofit.data.remote.RetrofitClient;
import com.example.retrofit.data.remote.SOService;

public class ApiUtils {

        public static final String BASE_URL = "http://schedulekma.herokuapp.com/api/";

        public static SOService getSOService() {
            return RetrofitClient.getClient(BASE_URL).create(SOService.class);
        }
    }

