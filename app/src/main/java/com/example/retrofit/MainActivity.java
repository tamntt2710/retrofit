package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.retrofit.data.model.Lesson;
import com.example.retrofit.data.model.SOAnswersResponse;
import com.example.retrofit.data.model.Schedule;
import com.example.retrofit.data.remote.RetrofitClient;
import com.example.retrofit.data.remote.SOService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    private SOService mSOService;
    private DataAdapter adapter;
    private RecyclerView recyclerView;
    private SOAnswersResponse soAnswersResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.card_recycler_view);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        mSOService = ApiUtils.getSOService();

        mSOService.getAnswers().enqueue(new Callback<SOAnswersResponse>() {
            @Override
            public void onResponse(Call<SOAnswersResponse> call, Response<SOAnswersResponse> response) {
                Log.e("Main Activity", "code: " + response.code());
                Log.e("Main Activity", "code: " + response.body().toString());
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this,"Error " ,Toast.LENGTH_SHORT).show();
                    return;
                }

                 SOAnswersResponse soAnswersResponse = response.body();

                ArrayList<Lesson> lessonList = new ArrayList<>();
                lessonList.addAll(soAnswersResponse.getSchedule().get(0).getLessons());


                adapter = new DataAdapter(lessonList);
                recyclerView.setAdapter(adapter);
                  adapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,"Success",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<SOAnswersResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error" + t.getMessage(),Toast.LENGTH_SHORT).show();
            }

        });




    }
}
